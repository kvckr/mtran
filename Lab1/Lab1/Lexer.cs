﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using static System.Console;

namespace Lab1
{

    public enum Type
    {
        Literal = 0,
        Reserved,
        Splitter,
        Id
    }

    public class Token
    {
        public Type TokenType { get; set; }
        public string TokenContent { get; set; }

        public Token(Type tokenType, string tokenContent)
        {
            TokenType = tokenType;
            TokenContent = tokenContent;
        }

        public override string ToString()
        {
            return TokenContent;
        }
    }

    public class Error
    {
        private string text;
        private int column;
        private int row;

        public Error(string text, int column, int row)
        {
            this.text = text;
            this.column= column;
            this.row = row;
        }

        public override string ToString()
        {
            string result = $"Error at {column} column and {row} row.\n";
            result = result + text;
            return result;
        }
    }

    public class Lexer
    {
        public Lexer(string s)
        {
            Regex commentRegex = new Regex(@"/\\*(?:.|[\\n\\r])*?\\*/");
            if (commentRegex.IsMatch(s))
            {
                code = commentRegex.Replace(s, "");
            }
            else
            {
                code = s;
            }
        }

        private string code;
        private StringBuilder currentToken;

        public int CurrentRow { get; set; } = 1;
        public int CurrentColumn { get; set; } = 1;
        public int CurrentIndex { get; set; } = 0;
        private List<string> splittersList = new List<string>()
        { "(", ")", "{", "}", "==","!=", "+", "&", "%", ";", "=", ",","<",">","/","*"," "};
        private List<string> reservedList = new List<string>()
        { "continue", "break", "void", "char", "int","if","return" };
        public List<Token> tokens = new List<Token>();
        private int currentIndex = -1;

        private List<Error> errors = new List<Error>();

        public Token GetNextToken()
        {
            if (currentIndex < tokens.Count - 1)
            {
                currentIndex++;
                return tokens[currentIndex];
            }
            return null;
        }

        public Token GetPreviousToken()
        {
            if (currentIndex > 0)
            {
                currentIndex--;
                return tokens[currentIndex];
            }
            return null;
        }

        public void PrintCode()
        {
            Write(code);
        }

        public void AddToken(string token)
        {
            if (String.IsNullOrWhiteSpace(token))
                return;
            if (splittersList.Contains(token))
            {
                tokens.Add(new Token(Type.Splitter, token));
                return;
            }
            if (reservedList.Contains(token))
            {
                tokens.Add(new Token(Type.Reserved, token));
            }
            else
            {
                int tmp;
                if (int.TryParse(token, out tmp))
                {
                    tokens.Add(new Token(Type.Literal, token));
                }
                else { tokens.Add(new Token(Type.Id, token)); }

            }
        }

        public bool TryParseString()
        {
            AppendChar(code[CurrentIndex]);
            CurrentIndex++;
            for (; CurrentIndex < code.Length; CurrentIndex++)
            {
                switch (code[CurrentIndex])
                {
                    case '\r':
                        errors.Add(new Error("Unexpected new line in string literal", CurrentColumn, CurrentRow));
                        CurrentColumn = 0;
                        CurrentRow += 1;
                        break;
                    case '"':
                        AppendChar(code[CurrentIndex]);
                        tokens.Add(new Token(Type.Literal, currentToken.ToString()));
                        currentToken.Clear();
                        return true;
                        break;
                    default:
                        AppendChar(code[CurrentIndex]);
                        break;
                }
            }
            return false;
        }

        public bool TryParseDirective()
        {
            AppendChar(code[CurrentIndex]);
            CurrentIndex++;
            bool isincludeStarted = false;
            for (; CurrentIndex < code.Length; CurrentIndex++)
            {
                switch (code[CurrentIndex])
                {
                    case '\r':
                        errors.Add(new Error("Unexpected new line in directive", CurrentColumn, CurrentRow));
                        CurrentColumn = 0;
                        CurrentRow += 1;
                        break;
                    case ' ':
                        AppendChar(code[CurrentIndex]);
                        tokens.Add(new Token(Type.Reserved, currentToken.ToString()));
                        currentToken.Clear();
                        break;
                    case '<':
                        if (isincludeStarted)
                        {
                            errors.Add(new Error("Invalid header name", CurrentColumn, CurrentRow));
                        }
                        AppendChar(code[CurrentIndex]);
                        isincludeStarted = true;
                        break;
                    case '>':
                        if (isincludeStarted)
                        {
                            if (code[CurrentIndex + 1] != '\r')
                            {
                                errors.Add(new Error("Invalid header name", CurrentColumn, CurrentRow));
                            }
                            AppendChar(code[CurrentIndex]);
                            tokens.Add(new Token(Type.Id, currentToken.ToString()));
                            currentToken.Clear();
                            return true;
                        }
                        errors.Add(new Error("Invalid header name", CurrentColumn, CurrentRow));
                        break;
                    default:
                        AppendChar(code[CurrentIndex]);
                        break;
                }
            }
            return true;
        }

        public void AppendChar(char ch)
        {
            currentToken.Append(ch);
            CurrentColumn += 1;
        }

        public void ParseSplitter()
        {
            AddToken(currentToken.ToString());
            currentToken.Clear();
            AppendChar(code[CurrentIndex]);
            AddToken(currentToken.ToString());
            currentToken.Clear();
        }

        public void Parse()
        {
            currentToken = new StringBuilder();
            bool skipSplitter = false;
            for (CurrentIndex = 0; CurrentIndex < code.Length; CurrentIndex++)
            {
                switch (code[CurrentIndex])
                {
                    case '\r':
                        AddToken(currentToken.ToString());
                        currentToken.Clear();
                        break;
                    case '\n':
                        CurrentRow += 1;
                        CurrentColumn = 1;
                        break;
                    case '#':
                        if (!TryParseDirective())
                        {
                            return;
                        }
                        break;
                    case '\t':
                    case ';':
                    case '(':
                    case ')':
                    case '{':
                    case '}':
                    case '&':
                    case ' ':
                    case ',':
                        if (!skipSplitter)
                        {
                            ParseSplitter();
                        }
                        skipSplitter = false;
                        break;
                    case '/':
                    case '*':
                        if (code[CurrentIndex + 1] == '*')
                        {
                            errors.Add(new Error("Unclosed comment", CurrentColumn, CurrentRow));
                        }
                        else
                        {
                            if (!skipSplitter)
                            {
                                ParseSplitter();
                            }
                            skipSplitter = false;
                        }
                        break;
                    case '=':
                    case '!':
                        if (code[CurrentIndex + 1] == '=')
                        {
                            AddToken(currentToken.ToString());
                            currentToken.Clear();
                            AppendChar(code[CurrentIndex]);
                            AppendChar(code[++CurrentIndex]);
                            AddToken(currentToken.ToString());
                            currentToken.Clear();
                            if (splittersList.Contains(code[CurrentIndex + 1].ToString()))
                            {
                                skipSplitter = true;
                            }
                        }
                        else
                        {
                            ParseSplitter();
                        }
                        break;
                    case '"':
                        currentToken.Clear();
		                TryParseString();

                        break;
                    default:
                        AppendChar(code[CurrentIndex]);
                        break;
                }
            }
            //if (errors.Count == 0)
            //{
            //    WriteLine(new String('=', 79));
            //    WriteLine("Идентификаторы");
            //    WriteLine(new String('=', 79));

            //    foreach (var token in tokens.Where(x => x.TokenType.Equals(Type.Id)))
            //    {
            //        WriteLine(token);
            //    }
            //    WriteLine(new String('=', 79));
            //    WriteLine("Литералы");
            //    WriteLine(new String('=', 79));
            //    foreach (var token in tokens.Where(x => x.TokenType.Equals(Type.Literal)))
            //    {
            //        WriteLine(token);
            //    }
            //    WriteLine(new String('=', 79));
            //    WriteLine("Ключевые слова");
            //    WriteLine(new String('=', 79));
            //    foreach (var token in tokens.Where(x => x.TokenType.Equals(Type.Reserved)))
            //    {
            //        WriteLine(token);
            //    }
            //    WriteLine(new String('=', 79));
            //    WriteLine("Разделители");
            //    WriteLine(new String('=', 79));
            //    foreach (var token in tokens.Where(x => x.TokenType.Equals(Type.Splitter)))
            //    {
            //        WriteLine(token);
            //    }
            //}
            //else
            //{
            //    foreach (var error in errors)
            //    {
            //        WriteLine(error);
            //    }
            //}

        }
    }
}
