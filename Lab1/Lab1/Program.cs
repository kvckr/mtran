﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using static System.Console;

namespace Lab1
{
    public class Program
    {
        static void Main(string[] args)
        {
            string s = File.ReadAllText("in.txt");
            var lexer = new Lexer(s);
            Parser parser = new Parser(lexer);
            parser.Parse();
        }
    }
}
