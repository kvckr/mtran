﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using NGenerics.DataStructures.Trees;
using NGenerics.Extensions;

namespace Lab1
{
    public class Parser
    {

        public List<string> ToPrefix(string[] tokens)
        {
            Stack<string> s = new Stack<string>();
            List<string> outputList = new List<string>();
            int n;
            foreach (string c in tokens)
            {
                if (int.TryParse(c.ToString(), out n))
                {
                    outputList.Add(c);
                }
                if (c == "(")
                {
                    s.Push(c);
                }
                if (c == ")")
                {
                    while (s.Count != 0 && s.Peek() != "(")
                    {
                        outputList.Add(s.Pop());
                    }
                    s.Pop();
                }
                if (isOperator(c) == true)
                {
                    while (s.Count != 0 && Priority(s.Peek()) >= Priority(c))
                    {
                        outputList.Add(s.Pop());
                    }
                    s.Push(c);
                }
            }
            while (s.Count != 0)//if any operators remain in the stack, pop all & add to output list until stack is empty 
            {
                outputList.Add(s.Pop());
            }
            return outputList;
        }

        static int Priority(string c)
        {
            if (c == "^")
            {
                return 3;
            }
            else if (c == "*" || c == "/" || c == "%")
            {
                return 2;
            }
            else if (c == "+" || c == "-")
            {
                return 1;
            }
            else {
                return 0;
            }
        }
        static bool isOperator(string c)
        {
            if (c == "+" || c == "-" || c == "*" || c == "/" || c == "^" || c == "%")
            {
                return true;
            }
            else {
                return false;
            }
        }
        private Lexer lexer;

        private BinaryTree<Token> tree = new BinaryTree<Token>(new Token(Type.Literal, "base"));


        public Parser(Lexer lexer)
        {
            this.lexer = lexer;
            lexer.Parse();
            current = tree;
        }

        private List<string> binaryOperators = new List<string>() { "+", "-", "=", "%", "(", ")" };

        private BinaryTree<Token> current;

        private Token emptyToken = new Token(Type.Literal, "");

        public void TryParseExpression()
        {
            while (true)
            {
                Token token = lexer.GetNextToken();
                if (token == null)
                {
                    break;
                }

            }

        }

        public class Variable
        {
            public string Name { get; set; }

            public string Type { get; set; }
        }

        private List<Variable> variables = new List<Variable>() {
            new Variable() {Name = "rand"}, new Variable() { Name = "NULL" }, new Variable() { Name = "srand" },
            new Variable() {Name = "puts"},new Variable() {Name = "time"},new Variable() {Name = "scanf"},
        };
        public void Parse()
        {
            Process p = new Process();
            p.StartInfo.FileName = "C:\\Users\\mrkva\\AppData\\Local\\Programs\\Python\\Python35-32\\python.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.Arguments = "test.py";
            p.Start();
            string s = File.ReadAllText("in.txt");
            Regex commentRegex = new Regex(@"/\\*(?:.|[\\n\\r])*?\\*/");
            s = commentRegex.Replace(s, "");
            Regex dirRegex = new Regex(@"#include[^\r\n]+\r\n");
            s = dirRegex.Replace(s, "\r\n");
            File.WriteAllText("test.c", s);
            s = File.ReadAllText("otherin.txt");
            s = commentRegex.Replace(s, "");
            s = dirRegex.Replace(s, "\r\n");
            File.WriteAllText("othertest.c", s);
            StreamReader q = p.StandardOutput;
            String output = q.ReadToEnd();
            StreamReader z = p.StandardError;
            String outputerr = z.ReadToEnd();
            if (outputerr != "")
            {
                var regex = new Regex(@"pycparser.plyparser.ParseError: *.+");
                var abc = regex.Match(outputerr);
                var bca = abc.ToString().Replace("pycparser.plyparser.ParseError: :", "");
                string[] strs = bca.Split(':');
                WriteLine($"Error at {strs[0]} row and {strs[1]} column");
            }
            else {
                string[] lines = output.Split('\n');

                var varreg = new Regex(@"TypeDecl: *.+");

                var varasigreg = new Regex(@"Constant: *.+");

                var idreg = new Regex(@"ID: *.+");

                var divreg = new Regex(@"BinaryOp: //*.+");

                var constreg = new Regex(@"Constant: *.+");

                var anyreg = new Regex(@"BinaryOp: *.+");


                int i;
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    //SUPER PARSER
                    string[] codetest = File.ReadAllText("test.c").Split('\n');
                    string[] othercodetest = File.ReadAllText("othertest.c").Split('\n');

                    for (int lc = 0; lc < codetest.Length; lc++)
                    {
                        if (codetest[lc] != othercodetest[lc])
                        {
                            WriteLine($"Error at line {lc + 1}");
                            ReadKey();
                            return;
                        }
                    }

                }
                else {
                    for (i = 0; i < lines.Length; i++)
                    {
                        if (varreg.IsMatch(lines[i]))
                        {
                            variables.Add(new Variable() { Name = lines[i].Split(':')[1].Split(',')[0].Replace(" ", ""), Type = lines[i + 1].Split('\'')[1] });
                            i++;
                            if (varasigreg.IsMatch(lines[i + 1]))
                            {
                                if (variables.Last().Type != lines[i + 1].Split(':')[1].Split(',')[0].Replace(" ", ""))
                                {
                                    //WriteLine("Wrong type");
                                    string[] code = File.ReadAllText("test.c").Split('\n');

                                    for (int cn = 0; cn < code.Length; cn++)
                                    {
                                        if (code[cn].Contains(variables.Last().Name))
                                        {
                                            WriteLine($"Error at line {cn + 1}");
                                            return;
                                        }
                                    }
                                    i++;
                                }
                            }
                        }
                        if (idreg.IsMatch(lines[i]))
                        {
                            if (!variables.Select(x => x.Name).Contains(lines[i].Split(':')[1].Replace(" ", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "")))
                            {
                                //WriteLine($"Undeclared variable {lines[i].Split(':')[1].Replace(" ", "")}");
                                string[] code = File.ReadAllText("test.c").Split('\n');

                                for (int cn = 0; cn < code.Length; cn++)
                                {
                                    if (code[cn].Contains(lines[i].Split(':')[1].Replace(" ", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "")))
                                    {
                                        WriteLine($"Error at line {cn + 1}");
                                        return;
                                    }
                                }
                            }
                        }

                        if (divreg.IsMatch(lines[i]))
                        {
                            if (constreg.IsMatch(lines[i + 2]))
                            {
                                if (int.Parse(lines[i + 2].Split(',')[1].Replace(" ", "")) == 0)
                                {
                                    //WriteLine("Division by zero");
                                    string[] code = File.ReadAllText("test.c").Split('\n');

                                    for (int cn = 0; cn < code.Length; cn++)
                                    {
                                        if (code[cn].Contains(@"/ 0"))
                                        {
                                            WriteLine($"Error at line {cn + 1}");
                                            return;
                                        }
                                    }
                                }
                            }
                        }

                        if (anyreg.IsMatch(lines[i]))
                        {
                            string fid = "";
                            string sid = "";
                            if (idreg.IsMatch(lines[i + 1]))
                            {
                                Variable v = variables.First(x => x.Name == lines[i + 1].Split(':')[1].Replace(" ", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", ""));
                                fid = v.Type;
                            }
                            if (idreg.IsMatch(lines[i + 2]))
                            {
                                Variable v = variables.First(x => x.Name == lines[i + 2].Split(':')[1].Replace(" ", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", ""));
                                sid = v.Type;
                            }
                            if (fid != sid && !string.IsNullOrEmpty(fid) && !string.IsNullOrEmpty(sid))
                            {
                                //WriteLine("Type mismatch");
                                WriteLine($"Error at line {20 + 1}");
                                return;
                            }
                        }


                    }
                }

            }

            p.WaitForExit();
            ReadKey();

        }

    }
}
